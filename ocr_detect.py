#!/usr/bin/env python3
try: 
        from PIL import ImageGrab
        backend = None
except:
        import pyscreenshot as ImageGrab
        backend = 'imagemagick'
        
import pytesseract as ocr 
import sys
import re
import screen_draw

if (len(sys.argv) < 2):
        print("usage: {} notes".format(sys.argv[0]))
        exit(1)

        
notes_file = sys.argv[1]
keywords = []

#Open the note files and find all the keywords
with open(notes_file, "r") as file:
        line = file.readline()
        while line:
                line = file.readline()
                if '* ' == line[0:2]:
                        keywords.append(line[2:].rstrip("\n"))

print(keywords)
                        
img = ImageGrab.grab(backend=backend)
dimX,dimY = img.size

#Perform send to server here

####SERVER BLOCK####

data = ocr.image_to_boxes(img)
img.close()
#print(data)
# format string into string and list of dictionaries. 
data = data.split('\n')
str = ''
for i in range(len(data)):
        c,x,y,z,w,a = data[i].split(' ')
        str += c
        data[i] = {'char':c,
                   'x':int(x),
                   'w':dimY-int(w),        #tesseract has the y axis flipped from standard graphics
                   'z':int(z),
                   'y':dimY-int(y)}
        
#now match and find the coor
for keyword in keywords:
        pattern = re.compile(keyword)
        for match in pattern.finditer(str):
                start = data[match.start()]
                end = data[match.start()+len(match.group())-1]
                box = (start['x'], start['y'],
                       end['z']-start['x'], end['w']-start['y'])
                
                #Send coordinates back to client here

                #### END SERVER BLOCK ####
        
                screen_draw.highlight(start['x'],
                                      start['y'],
                                      end['z']-start['x'],
                                      end['w']-start['y'],
                                      "red")
                print("{} {} {}\n---------".format(match.start(), match.group(), data[match.start()]))
                #print(len(data))
